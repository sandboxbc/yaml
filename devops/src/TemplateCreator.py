#!/usr/bin/python3.8
import json
import yaml



# Read JSON
with open('resources/header.json') as f:
  header = json.load(f)
with open('resources/parameters.json') as f:
  parameters = json.load(f)
with open('resources/objects.json') as f:
  objects = json.load(f)

# Create template
template = {**header, **parameters, **objects}


#Load config
with open('resources/config.json') as f:
  config = json.load(f)

for target in config.values():
  for envKVP in target:
    for env in envKVP.values():
      # Write YAML
      f = open('../out/' + env + '.yaml', 'w')
      f.write(yaml.dump(template))
      f.close()





